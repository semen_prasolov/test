package ru.vktest.loader.model;

public class User {

    public String vkUrl;
    public String vkImageUrl;
    public String firstName;
    public String lastName;

    public String getFullName() {
        return firstName + " " + lastName;
    }

}
