package ru.vktest.loader.service;

import com.vk.api.sdk.client.actors.UserActor;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.httpclient.HttpTransportClient;
import com.vk.api.sdk.objects.friends.UserXtrLists;
import com.vk.api.sdk.queries.users.UserField;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.vktest.loader.model.User;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class VkService {

    private UserActor actor;
    private VkApiClient vk;

    @Value("${userId}")
    private Integer userId;

    @Value("${token}")
    private String token;

    @PostConstruct
    void init() {

        actor = new UserActor(userId, token);
        vk = new VkApiClient(HttpTransportClient.getInstance());
    }

    public List<User> getFriends(Integer userId) {

        List<User> result = new ArrayList<User>();
        List<UserXtrLists> apiResult;

        try {

            List<UserField> fields = Arrays.asList(UserField.SCREEN_NAME, UserField.PHOTO_200);

            apiResult = vk.friends()
                    .get(actor, fields)
                    .userId(userId)
                    .execute()
                    .getItems();

        } catch (Exception e) {
            System.out.println("UNABLE TO GET FRIENDS FOR USER " + userId);
            return result;
        }

        apiResult.forEach( vkUser ->
                result.add(makeUserFromVkUser(vkUser))
        );

        return result;
    }

    public User makeUserFromVkUser(UserXtrLists vkUser) {

        User user = new User();

        user.firstName = vkUser.getFirstName();
        user.lastName = vkUser.getLastName();
        user.vkImageUrl = vkUser.getPhoto200();
        user.vkUrl = new StringBuilder("https://vk.com/").append(getId(vkUser)).toString();

        return user;
    }

    private String getId(UserXtrLists vkUser) {

        String id;

        if (vkUser.getScreenName() != null) {
            id = vkUser.getScreenName();
        } else {
            id = "id" + vkUser.getId();
        }

        return id;
    }


}
