package ru.vktest.loader.ui;

import com.vaadin.event.ShortcutAction;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;

@SpringUI
public class MainUI extends UI {

    private VerticalLayout root;

    @Autowired
    private FriendsLayout friendsLayout;

    @Override
    protected void init(VaadinRequest vaadinRequest) {

        setupLayout();
        addHeader();
        addSearchForm();
        addFriendsLayout();
    }

    private void addFriendsLayout() {
        root.addComponent(friendsLayout);
    }

    private void addSearchForm() {

        HorizontalLayout formLayout = new HorizontalLayout();

        TextField vkId = new TextField();
        vkId.setPlaceholder("Vk user ID");
        vkId.focus();

        Button findButton = new Button();
        findButton.setIcon(VaadinIcons.SEARCH);
        findButton.addClickListener(click -> {
            if (vkId.getValue().isEmpty()) {
                Notification.show("Enter user ID");
            } else {
                try {
                    friendsLayout.renderFriends(vkId.getValue());
                } catch (NumberFormatException nfe) {
                    Notification.show("ID must be a number");
                }
            }
        });
        findButton.setClickShortcut(ShortcutAction.KeyCode.ENTER);

        formLayout.addComponentsAndExpand(vkId);
        formLayout.addComponent(findButton);
        formLayout.setWidth("30%");
        root.addComponent(formLayout);
    }

    private void addHeader() {

        Label header = new Label("Vk friends");
        header.addStyleName(ValoTheme.LABEL_H1);
        root.addComponent(header);
    }

    private void setupLayout() {

        root = new VerticalLayout();
        root.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        setContent(root);
    }
}
