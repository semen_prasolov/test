package ru.vktest.loader.ui;

import com.vaadin.ui.HorizontalLayout;
import ru.vktest.loader.model.User;

import java.util.List;

public class FriendsRow extends HorizontalLayout {

    public FriendsRow(List<User> friends) {

        for (User friend: friends) {
            addComponent(new FriendLayout(friend));
        }
    }
}
