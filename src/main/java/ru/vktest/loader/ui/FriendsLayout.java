package ru.vktest.loader.ui;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.VerticalLayout;
import org.springframework.beans.factory.annotation.Autowired;
import ru.vktest.loader.model.User;
import ru.vktest.loader.service.VkService;

import javax.annotation.PostConstruct;
import java.util.List;

@SpringComponent
public class FriendsLayout extends VerticalLayout {

    @Autowired
    VkService vkService;

    @PostConstruct
    void init() {

        setWidth("80%");
    }


    void renderFriends(String userId) throws NumberFormatException {

        removeAllComponents();
        List<User> friends = vkService.getFriends(Integer.parseInt(userId));

        if (friends.isEmpty()) {
            Notification.show("User not found or has no friends");
        }

        for (int i = 0; i < friends.size(); i += 5) {
            addComponent(new FriendsRow(friends.subList(i, Math.min(i+5, friends.size()))));
        }
    }
}
