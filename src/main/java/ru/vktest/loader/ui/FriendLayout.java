package ru.vktest.loader.ui;

import com.vaadin.server.ExternalResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Image;
import com.vaadin.ui.Link;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import ru.vktest.loader.model.User;

public class FriendLayout extends VerticalLayout {

    public FriendLayout(User friend) {

        setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);

        Image avatar = new Image(null, new ExternalResource(friend.vkImageUrl));
        avatar.addClickListener(click -> {
           getUI().getPage().setLocation(friend.vkUrl);
        });

        Link link = new Link(friend.getFullName(), new ExternalResource(friend.vkUrl));

        addComponent(avatar);
        addComponent(link);
    }
}
